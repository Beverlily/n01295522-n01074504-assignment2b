﻿using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Assignment2B
{
    public partial class CodeBox : System.Web.UI.UserControl
    {
        private string code;
        private string owner;
        private DataView data;

        //List of strings containing code to display
        //JavaScript Code
        private List<string> myJavaScriptCode = new List<string>(new string[]
        {
            "for(var counter=1; counter<3; counter++){",
            "~sum=sum+1;",
            "~alert(sum);",
            "}",
            "alert(\"Total sum is: \" + sum);",
        });

        private List<string> webJavaScriptCode = new List<string>(new string[]
        {
            "var arr = [10, 11, 12, 13, 14];",
            "for (var i = 0; i< 5; i++)",
            "{",
            "~console.log(arr[i]);",
            "}",
        });

        //HTML/CSS code
        private List<string> myHTMLCSSCode = new List<string>(new string[]
        {
            "#exampleA {",
            "~border: 1px solid blue;",
            "~margin: 50px;",
            "}",
            "#exampleB {",
            "~border: 1px solid blue;",
            "~margin: 50px;",
            "}",
        });

        private List<string> webHTMLCSSCode = new List<string>(new string[]{
            "div {",
            "~border: 1px solid black;",
            "~margin-top: 100px;",
            "~margin-bottom: 100px;",
            "~margin-right: 150px;",
            "~margin-left: 80px;",
            "~background-color: lightblue;",
            "}",
        });

        //SQL code
        private List<string> mySQLCode = new List<string>(new string[]{
            "SELECT *",
            "FROM student",
            "INNER JOIN grade",
            "ON student.student_id = grade.student_id",
            "ORDER BY student.student_id ASC",
        });

        private List<string> mySQLCodeCont = new List<string>(new string[]
        {
            "SELECT *",
            "FROM student",
            "INNER JOIN grade",
            "ON student.student_id = grade.student_id",
            "ORDER BY student.student_id ASC",
            "WHERE course_mark>70 AND course_name = 'Web Programming'",
        });

        private List<string> webSQLCode = new List<string>(new string[]
        {
            "SELECT first_name, last_name, order_date, order_amount",
            "FROM customers c",
            "INNER JOIN orders o",
            "ON c.customer_id = o.customer_id",
        });

        public string Code
        {
            get { return code; }
            set { code = value; }
        }

        public string Owner
        {
            get { return owner; }
            set { owner = value; }
        }

        public void CreateCodeSource(List<string> codeToPrint)
        {
            /*Christine's code*/
            DataTable codedata = new DataTable();
            DataColumn index_col = new DataColumn();
            DataColumn code_col = new DataColumn();
            DataRow coderow;


            //Create Line Column
            index_col.ColumnName = "Line";
            index_col.DataType = System.Type.GetType("System.Int32");
            codedata.Columns.Add(index_col);


            //Create Code Column
            code_col.ColumnName = "Code";
            code_col.DataType = System.Type.GetType("System.String");
            codedata.Columns.Add(code_col);

            //Counter for number of lines 
            int i = 0;
            foreach (string code_line in codeToPrint)
            {
                //Value for Line Column
                coderow = codedata.NewRow();
                coderow[index_col.ColumnName] = i;

                //Value for Code Column
                string formatted_code = System.Net.WebUtility.HtmlEncode(code_line);
                formatted_code = formatted_code.Replace("~", "&nbsp;&nbsp;&nbsp;");
                coderow[code_col.ColumnName] = formatted_code;

                i++;
                codedata.Rows.Add(coderow);
            }
            data = new DataView(codedata); 
        }

        protected void Page_Load(object sender, EventArgs e)
        {
           //Making a DataView containing JavaScript code data
            if (code == "JavaScript")
            {
                if (owner == "Me")
                {
                    CreateCodeSource(myJavaScriptCode);
                }
                if (owner == "Web")
                {
                    CreateCodeSource(webJavaScriptCode);
                }
            }

            //Making a DataView containing SQL code data
            else if (code == "SQL")
            {
                if (owner == "Me")
                {
                    CreateCodeSource(mySQLCode);
                }
                if (owner == "Web")
                {
                    CreateCodeSource(webSQLCode);
                }
            }
            else if(code == "SQLCont")
            {
                CreateCodeSource(mySQLCodeCont);
            }

            //Making a DataView containing HTML/CSS code data
            else if (code == "HTMLCSS")
            {
                if (owner == "Me")
                {
                    CreateCodeSource(myHTMLCSSCode);
                }
                if (owner == "Web")
                {
                    CreateCodeSource(webHTMLCSSCode);
                }
            }

            //DataTable.DataSource ... makes DataView containing our code data as the DataTable's DataSource
            DisplayCode.DataSource = data;
            DisplayCode.DataBind();
        }
    }
}