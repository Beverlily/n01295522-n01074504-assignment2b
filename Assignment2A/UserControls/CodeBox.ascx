﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CodeBox.ascx.cs" Inherits="Assignment2B.CodeBox" %>
<div id="box">
   <!--can change the skin of CodeBox user control by changing skinID to code-light or code-dark-->
   <asp:DataGrid ID="DisplayCode" runat="server" SkinID="code-light" ></asp:DataGrid>
</div>
