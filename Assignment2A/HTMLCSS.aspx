﻿<%@ Page Title="HTML/CSS" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="HTMLCSS.aspx.cs" Inherits="Assignment2B.HTMLCSS" %>
<asp:Content ID="HTMLCSSContent" ContentPlaceHolderID="MainContent" runat="server">
   <div>
      <h1>HTML/CSS: Margin</h1>
      <p>The margin of a HTML element is the amount of space it has outside of its border.</p>
   </div>
</asp:Content>
<asp:Content ID="MyMarginExample" ContentPlaceHolderID="MyExample" runat="server">
     <h2>My Margin Example</h2>
     <div>
        <p id="exampleA">Hi I'm paragraph A, formed using the paragraph element tag 'p'. Right now I have default margin and padding.</p>
        <p id="exampleB">Hi I'm paragraph B, formed using the paragraph element tag 'p'. Right now I have default margin and padding.</p>
     </div>
     <br />
     <p> Right now, both paragraph A and B have the default margin and padding. Recall that margin is the amount of space an element has outside its border.</p>
     <p> So, if I want to add space between the two elements paragraph A and paragraph B, I could increase their margin.</p>
     <p> Lets increase both their margins by 50px by adding in the CSS <code>margin: 50px;</code> </p>
     <h3>CSS:</h3>

    <!-- UC to display my HTML/CSS margin example code-->
    <uctrl:CodeBox ID="My_HTMLCSS_Code" runat="server" code="HTMLCSS" owner="Me"></uctrl:CodeBox>

    <div>
       <p id="margin-exampleA">Hi I'm paragraph A, formed using the paragraph element tag 'p'. My margin is now 50px.</p>
       <p id="margin-exampleB">Hi I'm paragraph B, formed using the paragraph element tag 'p'. My margin is now 50px.</p>
    </div>
    <p>Now, you can see that there's 50px of space around all sides of paragraph A and paragraph B.</p>
</asp:Content>
<asp:Content ID="WebMarginPaddingExample" ContentPlaceHolderID="WebExample" runat="server">
   <div>
      <h2>Web Example: Margin</h2>
      <p>Here is another example of margin from <a href="https://www.w3schools.com/Css/css_margin.asp">W3Schools</a>.</p>
      <div id="w3-margin-example">This div element has a top margin of 100px, a right margin of 150px, a bottom margin of 100px, and a left margin of 80px.</div>
      <h3>CSS:</h3>
    
      <!-- UC to display the web HTML/CSS margin example code-->
      <uctrl:CodeBox ID="Web_HTMLCSS_Code" runat="server" code="HTMLCSS" owner="Web"></uctrl:CodeBox>

      <p>In this example, they've created a div element with different margin values for the top, bottom, left, and right side of the element.</p>
      <p>The div element they've created has 100px of space outside its border on the top side, 100px on the bottom side, 150px on the right side, and 80px on the left side. </p>
   </div>
    <br />
</asp:Content>
<asp:Content ID="UsefulHTMLCSSLinks" ContentPlaceHolderID="UsefulLinks" runat="server">
   <div class="links-container">
      <h2>More Learning Resources:</h2>
      <ul>
         <li><a href="https://medium.com/frontendshortcut/margin-vs-padding-c1fc8ea8bfaf">Medium</a></li>
         <li><a href="http://www.goer.org/HTML/intermediate/margins_and_padding/">The Pocket HTML Tutorial</a></li>
         <li><a href="https://www.digizol.com/2006/12/margin-vs-padding-css-properties.html">Digizol</a></li>
      </ul>
   </div>
</asp:Content>